﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Controller2D))]
public class PlayerMovement : MonoBehaviour
{
    public MirarOponente mir;

    [Header("NormalJump")]
	public float maxJumpHeight = 4;
	public float minJumpHeight = 1;
	public float timeToJumpApex = .4f;
	float accelerationTimeAirborne = .2f;
	float accelerationTimeGrounded = .1f;
	public float moveSpeed = 20;
    float movExtraRoot = 1;
    //public float moveSpeedWalking = 13;


    [Header("AirJump")]
    public bool canAirJump = true;
    public float fAirJump = 4;
    public int maxAirJump = 1;
    public int actAirJump = 0;

    [Header("Wall Jump & Slide")]
    public bool canWallSlide = true;
	public Vector2 wallJumpClimb;
	public Vector2 wallJumpOff;
	public Vector2 wallLeap;

	public float wallSlideSpeedMax = 3;
	public float wallStickTime = .25f;
	float timeToWallUnstick;

	float gravity;
	float maxJumpVelocity;
	float minJumpVelocity;
	Vector3 velocity;
	float velocityXSmoothing;

	Controller2D controller;

	Vector2 directionalInput;
	bool wallSliding = false;
	int wallDirX;

    bool enKnockBack = false;

    float knockBackdirX;
    int intensidadKB;

    public bool usingRootMotion = false;

    public bool corriendo = false;

    Vector2 rootMotionVel;


    public bool movePorAnimacion;
    public Transform tObjetivo;
    public float velMoveAnim = 10f;
    public Vector2 velAnim;
    Vector2 velCurrent;
    public bool moveTobjetivo = false;
    public bool movePorAnimacionRec = false;

    Animator anim;
    Collider2D col;
    void Start()
    {
        anim = GetComponent<Animator>();
		controller = GetComponent<Controller2D> ();
        mir = GetComponent<MirarOponente>();
        col = GetComponent<Collider2D>();
        gravity = -(2 * maxJumpHeight) / Mathf.Pow (timeToJumpApex, 2);
		maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
		minJumpVelocity = Mathf.Sqrt (2 * Mathf.Abs (gravity) * minJumpHeight);
	}

    void FixedUpdate()
    {
        if (usingRootMotion)
        {
            return;
        }

        CalculateVelocity();
        HandleWallSliding();

        Vector2 velFinal = new Vector2();

        if (movePorAnimacion)
        {
            velFinal = rootMotionVel * Time.fixedDeltaTime;
            velocity = rootMotionVel;
        }
        else if (movePorAnimacionRec)
        {
            velocity = tObjetivo.position - transform.position;
            velFinal = velocity * Time.fixedDeltaTime * 10;
        }
        else
        {
            velFinal = velocity * Time.fixedDeltaTime;
        }

        controller.Move(velFinal, directionalInput);
        anim.SetFloat("VelY", velFinal.y);



        if (controller.collisions.above || controller.collisions.below)
        {
            if (controller.collisions.slidingDownMaxSlope)
            {
                velocity.y += controller.collisions.slopeNormal.y * -gravity * Time.deltaTime;
            }
            else
            {
                velocity.y = 0;
            }
        }

        
    }

    public void SnapPosicion(Vector3 pos)
    {
        transform.position = pos;
    }    

    public void ActivarMovePorAnimRec(Transform x)
    {
        tObjetivo = x;
       // SnapPosicion(x.position);
        movePorAnimacionRec = true;
    }

    public void ActivarMovePorAnimExe()
    {
        movePorAnimacion = true;
    }

    public void DesactivarMovePorAnimacion()
    {
        movePorAnimacion = false;
        movePorAnimacionRec = false;
    }

    public void SetDirectionalInput (Vector2 input)
    {
        if(!enKnockBack)
        {
            directionalInput = input;
        }
    }

    public void SetDirectionalInput ()
    {
        if (!enKnockBack)
        {
            directionalInput = Vector2.Lerp(directionalInput, new Vector2(0, 0), .5f);
        }
    }

	public void SaltoNormalInputDown()
    {      
        if(enKnockBack)
        {            
            return;
        }
		if (wallSliding)
        {         
            actAirJump = 0;
            WallSlidingJump();
        }
        else if (controller.collisions.below)                                                           //Tocando Piso
        {
            actAirJump = 0;
			if (controller.collisions.slidingDownMaxSlope)
            {
                if (directionalInput.x != -Mathf.Sign (controller.collisions.slopeNormal.x))
                {                                                                                       // not jumping against max slope
					velocity.y = maxJumpVelocity * controller.collisions.slopeNormal.y;
					velocity.x = maxJumpVelocity * controller.collisions.slopeNormal.x;
				}
            }
            else
            {
				velocity.y = maxJumpVelocity;
			}
		}
        else if(canAirJump && actAirJump < maxAirJump)                                                  //Sin tocar piso
        {
            actAirJump++;
            velocity.y = maxJumpVelocity;
        }
	}
    
    void WallSlidingJump()
    {
        if(!canWallSlide)
        {
            return;
        }
        if (wallDirX == directionalInput.x)
        {
            velocity.x = -wallDirX * wallJumpClimb.x;
            velocity.y = wallJumpClimb.y;
        }
        else if (directionalInput.x == 0)
        {
            velocity.x = -wallDirX * wallJumpOff.x;
            velocity.y = wallJumpOff.y;
        }
        else
        {
            velocity.x = -wallDirX * wallLeap.x;
            velocity.y = wallLeap.y;
        }
    }

	public void SaltoNormalInputUp()
    {
		if (velocity.y > minJumpVelocity)
        {
			velocity.y = minJumpVelocity;
		}
	}		

	void HandleWallSliding()
    {
        if(!canWallSlide)
        {
            return;
        }
		wallDirX = (controller.collisions.left) ? -1 : 1;
		wallSliding = false;
		if ((controller.collisions.left || controller.collisions.right) && !controller.collisions.below && velocity.y < 0) {
			wallSliding = true;

			if (velocity.y < -wallSlideSpeedMax) {
				velocity.y = -wallSlideSpeedMax;
			}

			if (timeToWallUnstick > 0) {
				velocityXSmoothing = 0;
				velocity.x = 0;

				if (directionalInput.x != wallDirX && directionalInput.x != 0) {
					timeToWallUnstick -= Time.deltaTime;
				}
				else {
					timeToWallUnstick = wallStickTime;
				}
			}
			else {
				timeToWallUnstick = wallStickTime;
			}

		}

	}

	void CalculateVelocity()
    { 
        float targetVelocityX = directionalInput.x * moveSpeed + rootMotionVel.x;
        if (!corriendo) targetVelocityX /= 2;
		velocity.x = Mathf.SmoothDamp (velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below)?accelerationTimeGrounded:accelerationTimeAirborne);

        velocity.y += gravity * Time.fixedDeltaTime + rootMotionVel.y;      
	}       

    /// <summary>
    /// Aplica efecto KnockBack en el movimiento del personaje usando como referencia el punto de impacto(otherPos), la fuerza(fImpacto) y el tiempo 
    /// de inhabilitacion tBloqueo
    /// </summary>
    /// <param name="otherPos"> Posicion del punto de impacto </param>
    /// <param name="fImpacto"> Fuerza del Knockback</param>
    /// <param name="tBloqueo"> Duracion de la inhabilitacion del jugador</param>
    public void ActivarKnockBack(Vector3 otherPos, Vector2 fImpacto, float tBloqueo)
    {
        if (transform.position.x > otherPos.x) knockBackdirX = 1;
        else knockBackdirX = -1;
        directionalInput = new Vector3(knockBackdirX, 0).normalized;
        enKnockBack = true;
        StartCoroutine(StartKnockBack(tBloqueo, fImpacto,false));
    }

    public void ActivarKnockBack(Vector2 fImpacto, float tBloqueo)
    {      
        directionalInput = new Vector3(fImpacto.x, 0).normalized;
        enKnockBack = true;
        StartCoroutine(StartKnockBack(tBloqueo, fImpacto,true));
    }

    public void DesplazamientoSimple(Vector2 fImpacto, float tBloqueo)
    {
        knockBackdirX = 1;        
        if (!mir.mirandoDer) knockBackdirX = -1;       

        directionalInput = new Vector3(knockBackdirX, 0).normalized;
        enKnockBack = true;
        StartCoroutine(StartKnockBack(tBloqueo, fImpacto, false));
    }
    IEnumerator StartKnockBack(float tBloqueo, Vector2 fImpacto, bool dirSimple)
    {
        enKnockBack = true;
        if(dirSimple)
            StartCoroutine("KnockBackDirSimple", fImpacto);
        else
            StartCoroutine("KnockBack", fImpacto);
        yield return new WaitForSeconds(tBloqueo);
        enKnockBack = false;
        if (dirSimple)
            StopCoroutine("KnockBackDirSimple");
        else
            StopCoroutine("KnockBack");
    }
    IEnumerator KnockBack(Vector2 fImpacto)
    {
        velocity.y = fImpacto.y;
        while (true)
        {       
                velocity = new Vector2 (fImpacto.x * knockBackdirX * 50 * Time.deltaTime, velocity.y);           
            yield return null;
        }
    }
    IEnumerator KnockBackDirSimple(Vector2 fImpacto)
    {
        velocity.y = fImpacto.y;
        while (true)
        {
            velocity = new Vector2 (fImpacto.x * 50 * Time.deltaTime, velocity.y);        
            yield return null;
        }
    }

    public void InRootMotionMovement(Vector2 x)
    {
        if (mir.mirandoDer) rootMotionVel = x;
        else rootMotionVel = new Vector2(-x.x, x.y);
    }

    public void RecibirGrab(Transform tObj)
    {
        mir.Enfrentar();
        GetComponent<Animator>().SetTrigger("GrabRecibir");
        tObjetivo = tObj;
        ActivarMovePorAnimRec(tObj);
    }

    public void RecibirGrabAereo(Transform tObj)
    {
        mir.Enfrentar();
        GetComponent<Animator>().SetTrigger("GrabRecibirAereo");
        tObjetivo = tObj;
        ActivarMovePorAnimRec(tObj);
    }
    
    public void TerminarGrab()
    {
        DesactivarMovePorAnimacion();
    }

    IEnumerator IniciarReposicionamiento(Vector3 posObj)
    {
        enKnockBack = true;
        Vector3 dir = posObj - transform.position;
        posObj = posObj + dir.normalized * 2.5f;
        StartCoroutine("Reposicionamiento", posObj);
        yield return new WaitForSeconds(.8f);
        enKnockBack = false;
        StopCoroutine("Reposicionamiento");
    }

    IEnumerator Reposicionamiento(Vector2 posObj)
    {
        while (true)
        {
            Vector3.Lerp(transform.position, posObj, .2f);
            yield return null;
        }
    }

    public void resetVel()
    {
        velocity = Vector2.zero;
        velAnim = Vector2.zero;
    }
}
