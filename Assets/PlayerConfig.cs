﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerConfig : MonoBehaviour
{
    public enum playerNum { _1, _2, _3, _4 };
    public playerNum jugador;

    PlayerMovement pMov;
    Controller2D ctrl2D;
    PlayerInput pInput;
    MirarOponente mir;
    SetDeMov setMov;
    VidaJugador vida;
    ControlDirPuntoDisparo cDisp;
    LayerMask playerMask;

    private void Awake()
    {
        pMov = GetComponent<PlayerMovement>();
        //ctrl2D = GetComponent<Controller2D>();
        pInput = GetComponent<PlayerInput>();
        mir = GetComponent<MirarOponente>();
        //setMov = GetComponent<SetDeMov>();
        //vida = GetComponent<VidaJugador>();
        cDisp = GetComponentInChildren<ControlDirPuntoDisparo>();
    }

    public Transform tEnemy;

    public enum tipoControl {Libre, ObjetivoFijo};
    public tipoControl apuntar;

    public void InicializarConfigJugador()
    {
        pInput.jugador = this.jugador;
        pInput.SetMask(this.playerMask);
        AplicarTipoControl();
    }
    public void SetMask(LayerMask x)
    {
        playerMask = x;
    }
    void AplicarTipoControl()
    {
        cDisp.tEnemigo = tEnemy;
        if (apuntar == tipoControl.Libre)
        {
            mir.ActivarMirLibre();
            cDisp.seguirEnemigo = false;
        }
        else
        {
            mir.ActivarMirSemiAuto(tEnemy);
            cDisp.seguirEnemigo = true;
        }

    }

    public void SetOponente(Transform enemy)
    {
        tEnemy = enemy;
    }

    public void SetIdJugador(int x)
    {
        if(x>=5)
        {
            Debug.LogError("Error identificacion superior a la cantidad de jugadores permitidos (4 max)");
            return;
        }
        print(gameObject.name + this.jugador);
        this.jugador = (playerNum)(x-1);
        print(gameObject.name + this.jugador);

    }
}
