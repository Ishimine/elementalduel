﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerControlSimple : NetworkBehaviour
{

    public float velMov = 10;
    [Header("Visual")]    
    public SpriteRenderer render;
    public Transform cuerpo;
    public GameObject proyectil;
    public Transform puntoDisparo;
    public Transform barraVida;    
    private Animator anim;
    [SerializeField] private bool mirandoDer = true;


    [Header("Disparos")]
    public float contDisp;
    public float cadenciaDisp = 0.3f;


    [Header("Fisica")]
    public float velProjectil = 20;
    private Rigidbody2D rb;
    private Collider2D col;

    public Vector2 fSaltoSimple = new Vector2(5, 2);


    [Header("Chequeos de terreno")]
    public LayerMask collisionMask;
    private float verticalRaySpacing;
    private float horizontalRaySpacing;
    public int horizontalRayCount;
    public int verticalRayCount;
    public float bordeCor;
    public Vector2 offsetRayos;
    public float yRayLength;
    public float xRayLength;


    [SerializeField]    private bool enPiso;
    [SerializeField]    private bool enParedIzq;
    [SerializeField]    private bool enParedDer;

    public bool EnParedIzq()
    {
        if (!enPiso)
        {
            float directionX = -1;                               //DireccionDeLosRayos -1 lado izquiero 1 lado derecho

            horizontalRaySpacing = (col.bounds.size.y / 2) / (horizontalRayCount - 1);                                                    //Calcular distancia entre rayos
            for (int i = 0; i < horizontalRayCount; i++)
            {
                Vector2 rayOrigin = new Vector2(col.bounds.min.x, col.bounds.min.y) - Vector2.right * offsetRayos.x;                                    //Ubicar primera punta
                rayOrigin += Vector2.up * (horizontalRaySpacing * i);                                                  //Sumar cantidades de distancia de acuerdo al numero del rayo
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, xRayLength, collisionMask);
                Debug.DrawRay(rayOrigin, Vector2.right * directionX * xRayLength, Color.red);
                if (hit)
                {
                    return hit;
                }
            }
            return false;
        }
        else
        {
            return false;
        }
    }
    public bool EnParedDer()
    {
        if (!enPiso)
        {
            float directionX = 1; //DireccionDeLosRayos -1 lado izquiero 1 lado derecho         

            horizontalRaySpacing = (col.bounds.size.y / 2) / (horizontalRayCount - 1);                                                //Calcular distancia entre rayos
            for (int i = 0; i < horizontalRayCount; i++)
            {
                Vector2 rayOrigin = new Vector2(col.bounds.max.x, col.bounds.min.y) + Vector2.right * offsetRayos.x;             //Ubicar primera punta
                rayOrigin += Vector2.up * (horizontalRaySpacing * i);                                                             //Sumar cantidades de distancia de acuerdo al numero del rayo
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, xRayLength, collisionMask);
                Debug.DrawRay(rayOrigin, Vector2.right * directionX * xRayLength, Color.red);
                if (hit)
                {
                    return hit;
                }
            }
        }
        {
            return false;
        }
    }
    public bool EnPiso()
    {
        verticalRaySpacing = col.bounds.size.x / (verticalRayCount - 1) - bordeCor;                                                  //Calcular distancia entre rayos

        for (int i = 0; i < verticalRayCount; i++)
        {
            Vector2 rayOrigin = new Vector2(col.bounds.min.x + bordeCor, col.bounds.min.y) + Vector2.up * offsetRayos.y;              //Ubicar primera punta
            rayOrigin += Vector2.right * (verticalRaySpacing * i);                                                  //Sumar cantidades de distancia de acuerdo al numero del rayo
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.down, yRayLength, collisionMask);
            Debug.DrawRay(rayOrigin, Vector2.down * yRayLength, Color.red);
            if (hit)
            {
                return hit;
            }
        }
        return false;
    }

    

    void Start ()
    {
        anim = GetComponent<Animator>();
        col = GetComponent<Collider2D>();
        rb = GetComponent<Rigidbody2D>();
        if(!isLocalPlayer)
        {
           // rb.simulated = false;           
            name = name + "cliente";
            rb.gravityScale = 0;
        }
        
	}

    public override void OnStartLocalPlayer()
    {
        render.color = Color.cyan;
    }



    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        #region Chequeo Piso y Paredes
        enPiso = EnPiso();
        enParedDer = EnParedDer();
        enParedIzq = EnParedIzq();
        #endregion


        /*  #region Avance y Retroceso Simples
          if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.A) && enPiso)
          {
              if (mirandoDer) RetrocesoSimple();
              else AvanceSimple();
          }

          if (Input.GetKeyDown(KeyCode.D) && enPiso)
          {
              if (!mirandoDer) RetrocesoSimple();
              else AvanceSimple();
          }
          #endregion   */

        if (Input.GetAxis("Horizontal") != 0) MoverSimple();

        contDisp += Time.deltaTime;

        if (contDisp >= cadenciaDisp && (Input.GetButton("Fire1")))
        {
            CmdFire();
        }



        if (Input.GetAxis("Horizontal") > 0) CmdFlip();
        else if (Input.GetAxis("Horizontal") < 0) CmdFlip();
    }

    [Command]
    void CmdFlip()
    {
        RpcFlip();
    }

    [ClientRpc]
    void RpcFlip()
    {
        mirandoDer = !mirandoDer;
        transform.Rotate(0, 180, 0);
        barraVida.Rotate(0, 180, 0);
        //puntoDisparo.transform.parent.localScale = new Vector3 (-puntoDisparo.localScale.x, 1, 1);
        //cuerpo.localScale = new Vector3(-cuerpo.localScale.x, 1, 1);
    }

    void AvanceSimple()
    {
        anim.SetTrigger("Avance");
        rb.velocity = Vector2.zero;

        if (mirandoDer) rb.AddForce(fSaltoSimple, ForceMode2D.Impulse);
        else rb.AddForce(new Vector2(-fSaltoSimple.x, fSaltoSimple.y), ForceMode2D.Impulse);
    }

    void RetrocesoSimple()
    {
        anim.SetTrigger("Retroceso");
        rb.velocity = Vector2.zero;
        if(mirandoDer)   rb.AddForce(new Vector2(-fSaltoSimple.x,fSaltoSimple.y), ForceMode2D.Impulse);
        else rb.AddForce(fSaltoSimple, ForceMode2D.Impulse); ;
    }

    void MoverSimple()
    {
        rb.velocity = new Vector2(Input.GetAxisRaw("Horizontal") * velMov, rb.velocity.y);
    }

    [Command]
    void CmdFire()
    {
        contDisp = 0;
        // Create the Bullet from the Bullet Prefab
        GameObject clone = (GameObject)Instantiate(
            proyectil,
            puntoDisparo.position,
            puntoDisparo.localRotation);

        /*
        if(mirandoDer)  clone.GetComponent<Rigidbody2D>().velocity = clone.transform.right * velProyectil;
        else clone.GetComponent<Rigidbody2D>().velocity = clone.transform.right * -velProyectil;
        */


        if (mirandoDer) clone.GetComponent<Rigidbody2D>().velocity = clone.transform.right * clone.GetComponent<ProyectilSencillo>().vel;
        else clone.GetComponent<Rigidbody2D>().velocity = clone.transform.right * -clone.GetComponent<ProyectilSencillo>().vel;




        // Spawn the bullet on the Clients
        NetworkServer.Spawn(clone);

        // Destroy the bullet after 2 seconds
        Destroy(clone, 2.0f);
    }



}
