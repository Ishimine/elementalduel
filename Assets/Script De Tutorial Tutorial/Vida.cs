﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Vida : MonoBehaviour {

    public bool invulnerable = false;
    public int vidaMax = 100;
    public int vidaActual;

    public delegate void act(float x);
    public act actVida;

    public delegate void act2();
    public act2 objMuerto;


    private void Awake()
    {
        vidaActual = vidaMax;
    }
    
    public void RecibirDaño(int cant)
    {
        if (invulnerable)
            return;
        vidaActual -= cant;
        if (actVida != null)
        {
            actVida((float)vidaActual);
        }
        if (vidaActual <= 0)
        {
            vidaActual = 0;
            if(objMuerto != null) objMuerto();
            Muerto();
        }       
    }


    public void RecibirDaño(int cant, Vector2 posImpacto, Vector2 fImpacto, float durBloqueo, int intensidad)
    {
        GetComponent<MirarOponente>().Enfrentar();
        RecibirDaño(cant);
        KnockBack(posImpacto, fImpacto, durBloqueo);
        if (intensidad == 1)        
            GetComponent<Animator>().SetTrigger("KnockBack");         
        else if(intensidad == 2)
            GetComponent<Animator>().SetTrigger("KnockBackMedio");
        else         
            GetComponent<Animator>().SetTrigger("KnockBackFuerte");        
    }
    public void RecibirDaño(int cant, Vector2 fImpacto, float durBloqueo, int intensidad)
    {
        GetComponent<MirarOponente>().Enfrentar();
        RecibirDaño(cant);
        KnockBack(fImpacto, durBloqueo);
        if (intensidad == 1)
            GetComponent<Animator>().SetTrigger("KnockBack");
        else if (intensidad == 2)
            GetComponent<Animator>().SetTrigger("KnockBackMedio");
        else
            GetComponent<Animator>().SetTrigger("KnockBackFuerte");
    }


    abstract public void KnockBack(Vector2 posImpacto, Vector2 fImpacto, float durBloqueo, int dir = 0);
    abstract public void KnockBack(Vector2 fImpacto, float durBloqueo, int dir = 0);


    public abstract void Muerto();

}
