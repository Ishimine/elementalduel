﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarraVidaInGame : MonoBehaviour {


    public Vida vida;
    public Transform pivoteRelleno;

	void Start ()
    {
        vida.actVida += ActualizarVida;
    }

    void ActualizarVida(float x)
    {
        pivoteRelleno.transform.localScale = new Vector3(256/(100/x)/100, 1, 1);
	}

  
}
