﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ProyectilSencillo : NetworkBehaviour {

    public float vel;


    void Awake()
    {
        GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity * vel;
    }

    void OnCollisionEnter2D (Collision2D collision)
    {        
        var hit = collision.gameObject;
        var vida = hit.GetComponent<Vida>();
        if (vida != null)
        {
            vida.RecibirDaño(10);
        }
        Destroy(gameObject);
    }
}
