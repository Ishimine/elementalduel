﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadBox : MonoBehaviour {

	void OnCollisionEnter2D(Collision2D col)
    {
        var vida = col.gameObject.GetComponent<Vida>();

        if(vida != null)
        {
            vida.RecibirDaño(2000);
        }
        else Destroy(col.gameObject);
    }
}
