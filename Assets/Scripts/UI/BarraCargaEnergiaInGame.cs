﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarraCargaEnergiaInGame : MonoBehaviour {

    public SetDeMov set;
    public Transform pivoteRelleno;

    void Start()
    {
        set.actC += ActualizarBarra;
    }

    void ActualizarBarra(float x)
    {
        pivoteRelleno.transform.localScale = new Vector3(256 / (100 / x) / 100, 1, 1);
    }


}
