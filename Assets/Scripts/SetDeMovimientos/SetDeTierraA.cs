﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetDeTierraA : SetDeMov {

    public GameObject distPiedra_A;
    public GameObject distPiedra_B;
    public GameObject columna_A;
    public GameObject burstPiedra_A;
    public GameObject burstPiedra_B;
    public GameObject burstPiedra_C;

    public int cantSaltosAire;
    public int maxSaltos = 1;

    public override void Salto_In()
    {
        playerMov.SaltoNormalInputDown();
        anim.SetTrigger("Jump_In");
    }

    public override void Salto_Down()
    {
    }

    public override void Salto_Out()
    {
        playerMov.SaltoNormalInputUp();
    }

    public override void Fire1_In()
    {
        anim.SetTrigger("Fire1_In");
        anim.SetBool("Fire1_Down", true);        
    }

    public override void Fire1_Down()
    {
    }

    public override void Fire1_Out()
    {
        anim.SetTrigger("Fire1_Out");
        anim.SetBool("Fire1_Down", false);
    }

    public void Fire1Spawn()
    {
        GameObject proyectil = Instantiate<GameObject>(distPiedra_A, pDispFijo.transform.position, pDispFijo.transform.rotation);
        //proyectil.GetComponent<Proyectil>().creador = gameObject;
        proyectil.GetComponent<Proyectil>().tagDueño = gameObject.tag;
        proyectil.GetComponent<Proyectil>().Carga(cargaActual);
        cargaActual = 0;
        Destroy(proyectil, 5f);
    }

    public override void Fire2_In()
    {
        anim.SetTrigger("Fire2_In");
        anim.SetBool("Fire2_Down", true);
    }

    public override void Fire2_Down()
    {
        CargandoDown();
        // pInput.MovReducidoTotal(true);
    }

    public override void Fire2_Out()
    {
        anim.SetTrigger("Fire2_Out");
        anim.SetBool("Fire2_Down", false);
    }

    public void Fire2spawn()
    {
        GameObject proyectil = Instantiate<GameObject>(distPiedra_B, pDispDina.transform.position, pDispDina.transform.rotation);
        proyectil.GetComponent<Proyectil>().tagDueño = gameObject.tag;
        //proyectil.GetComponent<Proyectil>().Carga(cargaActual);
        proyectil.GetComponent<Proyectil>().creador = gameObject;
        cargaActual = 0;
        Destroy(proyectil, 5f);
    }

    public override void Fire3_In()
    {
        anim.SetTrigger("Fire3_In");
        anim.SetBool("Fire3_Down", true);
    }

    public override void Fire3_Down()
    {
    }

    public override void Fire3_Out()
    {
        anim.SetTrigger("Fire3_Out");
        anim.SetBool("Fire3_Down", false);
    }

    public void Fire3spawn(int tipo)
    {
        GameObject proyectil;
        switch (tipo)
        {
            case 1:
                proyectil = Crear(ref burstPiedra_A, pDispFijo, true);
                break;
            case 2:
                proyectil = Crear(ref burstPiedra_B, pDispFijo, true);
                proyectil.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
                break;
            case 3:
                proyectil = Crear(ref burstPiedra_C, pDispFijo, true);
                proyectil.transform.localScale = new Vector3(2.2f, 1.6f, 1.4f);
                proyectil.GetComponentInChildren<ColumnaDeAtaque_Tierra>().fImpacto = new Vector2(40, 40);
                break;
            default:
                proyectil = new GameObject();
                break;
        }
        proyectil.transform.Rotate(0, 0, 90);
        Destroy(proyectil, 10);
    }

    public override void AxisIzq(Vector2 xy)
    {
    }

    public override void R1_In()
    {
    }
    public override void R1_Down()
    {
    }
    public override void R1_Out()
    {
    }

    public override void L1_In()
    {
    }
    public override void L1_Down()
    {
    }
    public override void L1_Out()
    {
    }

    public override void L2_In()
    {
    }
    public override void L2_Down()
    {
    }
    public override void L2_Out()
    {
    }

    public override void R2_In()
    {
    }
    public override void R2_Down()
    {
    }
    public override void R2_Out()
    {
    }
    public override void AxisDer(Vector2 xy)
    {
    }
}
