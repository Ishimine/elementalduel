﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SetDeAireA : SetDeMov
{
    /// <summary>
    /// Recordar que clase padre contiene
    ///     Animator:        anim;
    ///     Transform:       puntoDisparo;
    ///     Controller2D:    ctrl2D;
    ///     PlayerMovement:  playerMov;
    ///     PlayerInput:      pInput
    ///     Collider2D:      col;
    /// </summary>
    /// 

    
    


    public GameObject corteDeViento;
    public GameObject corteDeViento2;
    public GameObject burstAire01;
    public GameObject burstAire02;
    public GameObject burstAire03;
    public GameObject burstAire04;
    public GameObject burstAire05;
    public GameObject burstAire06;
    public GameObject burstAire07;
    public GameObject burstAire08;
    public GameObject burstAire09;
    public GameObject burstAire10;

    public int cantSaltosAire;
    public int maxSaltos = 1;
    



    public  void Movimiento(float x, bool mirandoAdelante)
    {        

    }




    public void SpawnNormal(int tipo)
    {
        GameObject proyectil;
        switch (tipo)
        {
            case 1:
                proyectil = Crear(ref burstAire01, pDispFijo, true);     
                break;
            case 2:
                proyectil = Crear(ref burstAire02, pDispFijo, true);
                break;
            case 3:
                proyectil = Crear(ref burstAire03, pDispFijo, true);
                break;
            case 4:
                proyectil = Crear(ref burstAire04, pDispFijo, true);
                break;
            case 5:
                proyectil = Crear(ref burstAire05, pDispFijo, true);
                break;
            case 6:
                proyectil = Crear(ref burstAire06, pDispFijo, true);
                break;
            case 7:
                proyectil = Crear(ref burstAire07, pDispFijo, true);
                break;
            case 8:
                proyectil = Crear(ref burstAire08, pDispFijo, true);
                break;
            case 9:
                proyectil = Crear(ref burstAire09, pDispFijo, true);
                break;
            case 10:
                proyectil = Crear(ref burstAire10, pDispFijo, true);
                break;
            default:
                proyectil = new GameObject();
                break;
        }
        proyectil.transform.Rotate(0, 0, 90);
        Destroy(proyectil, 5f);
    }
   

  




}
