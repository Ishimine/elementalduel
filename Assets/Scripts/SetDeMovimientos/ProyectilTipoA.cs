﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProyectilTipoA : Proyectil {

    public float vel = 30;
    Rigidbody2D rb;


    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.up * vel;
    }

    void OnCollisionEnter2D(Collision2D col)
    {        
        if(EsImpactoValido(col.gameObject.tag))
        {           
            var hit = col.gameObject;
            var vida = hit.GetComponent<Vida>();
            if (vida != null)
            {
                vida.RecibirDaño((int)daño, col.contacts[0].point, fImpacto, durBloqueo,intensidad);
            }
            Destroy(gameObject);
        }
    }
}
