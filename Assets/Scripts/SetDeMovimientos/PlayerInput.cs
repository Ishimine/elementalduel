﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SetDeMov))]
public class PlayerInput : MonoBehaviour
{
    public delegate void actInputXY(Vector2 xy);
    public event actInputXY actStickDer;
    public event actInputXY actStickIzq;




    
    public PlayerConfig.playerNum jugador;
    LayerMask playerMask;


    public bool isDummy = false;

    Animator anim;
    Controller2D ctrl2D;

    SetDeMov setMov;
    public MirarOponente mir;    
    PlayerMovement player;

    void Start()
    {        
        ctrl2D = GetComponent<Controller2D>();
        anim = GetComponent<Animator>();
        player = GetComponent<PlayerMovement>();
        setMov = GetComponent<SetDeMov>();
    }
  
    public void SetMask(LayerMask x)
    {
        playerMask = x;
    }
    public void InicializarConfigJugador()
    {        
        ctrl2D.collisionMask = ctrl2D.collisionMask | playerMask;
    }
    public void Update()
    {
        EnviarInputsAlSetDeMovPlayer(jugador.ToString());
    }


    public void EnviarInputsAlSetDeMovPlayer(string p)
    {

        Vector2 stickDer = new Vector2(Input.GetAxis("Stick_H_Der" + p), Input.GetAxis("Stick_V_Der" + p));
        if (actStickDer != null) actStickDer(stickDer);
        setMov.AxisDer(stickDer);
        Vector2 stickIzq = new Vector2(Input.GetAxis("Horizontal" + p), Input.GetAxis("Vertical" + p));
        if (actStickIzq != null) actStickIzq(stickIzq);
        setMov.AxisIzq(stickIzq);

        if (Input.GetButtonDown("Jump" + p)) setMov.Salto_In();
        else if (Input.GetButton("Jump" + p)) setMov.Salto_Down();
        else if (Input.GetButtonUp("Jump" + p)) setMov.Salto_Out();

        if (Input.GetButtonDown("Fire1" + p)) setMov.Fire1_In();
        else if (Input.GetButton("Fire1" + p)) setMov.Fire1_Down();
        else if (Input.GetButtonUp("Fire1" + p)) setMov.Fire1_Out();

        if (Input.GetButtonDown("Fire2" + p)) setMov.Fire2_In();
        else if (Input.GetButton("Fire2" + p)) setMov.Fire2_Down();
        else if (Input.GetButtonUp("Fire2" + p)) setMov.Fire2_Out();

        if (Input.GetButtonDown("Fire3" + p)) setMov.Fire3_In();
        else if (Input.GetButton("Fire3" + p)) setMov.Fire3_Down();
        else if (Input.GetButtonUp("Fire3" + p)) setMov.Fire3_Out();

        if (Input.GetButtonDown("L1" + p)) setMov.L1_In();
        else if (Input.GetButton("L1" + p)) setMov.L1_Down();
        else if (Input.GetButtonUp("L1" + p)) setMov.L1_Out();

        if (Input.GetButtonDown("L2" + p)) setMov.L2_In();
        else if (Input.GetButton("L2" + p)) setMov.L2_Down();
        else if (Input.GetButtonUp("L2" + p)) setMov.L2_Out();

        if (Input.GetButtonDown("R1" + p)) setMov.R1_In();
        else if (Input.GetButton("R1" + p)) setMov.R1_Down();
        else if (Input.GetButtonUp("R1" + p)) setMov.R1_Out();

        if (Input.GetButtonDown("R2" + p)) setMov.R2_In();
        else if (Input.GetButton("R2" + p)) setMov.R2_Down();
        else if (Input.GetButtonUp("R2" + p)) setMov.R2_Out();
       
    }



    public void ActivarKnockBackFuerte()
    {
        anim.SetTrigger("KnockBackFuerte");
    }
}
