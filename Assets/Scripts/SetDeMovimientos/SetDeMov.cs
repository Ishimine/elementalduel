﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Collider2D), typeof(Controller2D), typeof(PlayerMovement))]
[RequireComponent(typeof(Animator))]
public abstract class SetDeMov : MonoBehaviour
{

    public delegate void actCarga(float x);
    public event actCarga actC;

    float cargaMax = 3;
    [SerializeField] public float cargaActual = 0;
    [SerializeField] float cargaVel = 1f;


    public GrabControl gControl;
    [HideInInspector]    public Animator anim;
    public Transform pDispFijo;
    public Transform pDispDina;
    [HideInInspector]    public Controller2D ctrl2D;
    [HideInInspector]    public PlayerMovement playerMov;
    [HideInInspector]    public Collider2D col;
    public MirarOponente mir;




    public GameObject proyectil_Simple;


    public PlayerInput pInput;

    public Vector2 fDash;
    public float velProyectil;    

    [HideInInspector]
    bool movBloqueado = false;
    [HideInInspector]
    bool movReducido = false;
    [HideInInspector]
    bool movReducidoTotal = false;
    
    public float tMovBloqueado = 3f;
    [SerializeField]
    private float contMovBloqueado;

    [System.Serializable]public struct ParametrosSalto
    {
        public string id;
        public Vector2 fuerza;
        public float tBloqueo;
    }
    

    [SerializeField]
    public ParametrosSalto[] DesplDash = new ParametrosSalto[3];
    [SerializeField]
    public ParametrosSalto[] DesplSalto = new ParametrosSalto[6];
    private int dirDesplazamiento = 1;
    private int dirDesplazamientoY = 1;



    public ParametrosSalto[] GrabKnockBack;


    Vector2 axisIzq;




    public struct ListadoBotones
    {
        public bool L1_Down;
        public bool L2_Down;
        public bool R1_Down;
        public bool R2_Down;
        public bool Fire1_down;
        public bool Fire2_down;
        public bool Fire3_down;
        public bool Jump_down;
        public bool Up_down;
        public bool Down_down;
        public bool Forward_down;
        public bool Back_down;



        void Reset()
        {
            L1_Down = false;
            L2_Down = false;
            R1_Down = false;
            R2_Down = false;
        }
    }

    public ListadoBotones botones;



    private void Awake()
    {
        ctrl2D = GetComponent<Controller2D>();
        playerMov = GetComponent<PlayerMovement>();
        col = GetComponent<Collider2D>();
        anim = GetComponent<Animator>();
        pInput = GetComponent<PlayerInput>();
    }

    public void Update()
    {
    }
            
    public virtual void Salto_In()
    {
        if (!botones.L1_Down && !movReducidoTotal)
        {
            playerMov.SaltoNormalInputDown();
        }
        anim.SetTrigger("Jump_In");
        botones.Jump_down = true;
    }
    public virtual void Salto_Down()
    {
        anim.SetBool("Jump_Down",true);
        botones.Jump_down = true;
    }
    public virtual void Salto_Out()
    {
        playerMov.SaltoNormalInputUp();
        botones.Jump_down = false;
    }

    public virtual void Fire1_In()
    {
        anim.SetTrigger("Fire1_In");
        anim.SetBool("Fire1_Down", true);
        botones.Fire1_down = true;
    }
    public virtual void Fire1_Down()
    {
        anim.SetBool("Fire1_Down",true);
        botones.Fire1_down = true;
    }
    public virtual void Fire1_Out()
    {
        anim.SetTrigger("Fire1_Out");
        anim.SetBool("Fire1_Down", false);
        botones.Fire1_down = false;
    }
 
    public virtual void Fire2_In()
    {
        anim.SetTrigger("Fire2_In");
        anim.SetBool("Fire2_Down", true);
        botones.Fire2_down = true;        
    }
    public virtual void Fire2_Down()
    {
        anim.SetBool("Fire2_Down", true);
    }
    public virtual void Fire2_Out()
    {
        anim.SetTrigger("Fire2_Out");
        anim.SetBool("Fire2_Down", false);
        botones.Fire2_down = false;
    }

    public virtual void Fire3_In()
    {
        anim.SetTrigger("Fire3_In");
        anim.SetBool("Fire3_Down", true);
        botones.Fire3_down = true;
    }
    public virtual void Fire3_Down()
    {
        anim.SetBool("Fire3_Down", true);
        botones.Fire3_down = true;
    }
    public virtual void Fire3_Out()
    {
        anim.SetTrigger("Fire3_Out");
        anim.SetBool("Fire3_Down", false);
        botones.Fire3_down = false;
    }

    public virtual void AxisIzq(Vector2 axis)
    {
        anim.SetFloat("AxisIzqY", axis.y);
        #region Mov Bloqueado
        if (movBloqueado)
        {
            contMovBloqueado += Time.deltaTime;
            if (contMovBloqueado > tMovBloqueado)
            {
                movBloqueado = false;
            }
            playerMov.SetDirectionalInput(new Vector2(0, 0));
            anim.SetFloat("VelX", 0);
            return;
        }
        #endregion

        movReducido = false;

        float velMod = 1;
        float dir = 1;
        //if (mir.mirandoDer && axis.x < 0) dir = 0.65f;
        if (mir.mirandoDer) anim.SetFloat("VelX", axis.x * dir);
        else anim.SetFloat("VelX", -axis.x * dir);

        axis.x = axis.x * dir;


        if (movReducidoTotal)
        {
            anim.SetFloat("VelX", 0);
            playerMov.SetDirectionalInput(new Vector2(0, 0));
        }
        else if (movReducido)
        {
            axis.x /= 2;
        }
        else
        {
            playerMov.SetDirectionalInput(axis * velMod);
        }

        axisIzq = axis;
      
    }

    public virtual void AxisDer(Vector2 axis)
    {
     
    }

    public virtual void R1_In()
    {
        anim.SetTrigger("R1_In");
        anim.SetBool("R1_Down", true);
        botones.R1_Down = true;
    }
    public virtual void R1_Down()
    {
        anim.SetBool("R1_Down", true);
        botones.R1_Down = true;
    }
    public virtual void R1_Out()
    {
        CargandoDown();
        anim.SetBool("R1_Down", false);
        anim.SetTrigger("R1_Out");
        botones.R1_Down = false;
    }
    public virtual void L1_In()
    {
        botones.L1_Down = true;
        anim.SetTrigger("L1_In");
        anim.SetBool("L1_Down", true);
        botones.L1_Down = true;
    }
    public virtual void L1_Down()
    {
        anim.SetBool("L1_Down", true);
        botones.L1_Down = true;
    }
    public virtual void L1_Out()
    {
        botones.L1_Down = false;
        anim.SetTrigger("L1_Out");
        anim.SetBool("L1_Down", false);
    }
    public virtual void L2_In()
    {
        botones.L2_Down = true;
        anim.SetTrigger("L2_In");
        anim.SetBool("L2_Down", true);
    }
    public virtual void L2_Down()
    {
        anim.SetBool("L2_Down", true);
    }
    public virtual void L2_Out()
    {
        botones.L2_Down = false;
        anim.SetTrigger("L2_Out");
        anim.SetBool("L2_Down", false);
    }


    public virtual void R2_In()
    {
        botones.R2_Down = true;
        anim.SetTrigger("R2_In");
        anim.SetBool("R2_Down", true);

    }
    public virtual void R2_Down()
    {
        anim.SetBool("R2_Down", true);
    }
    public virtual void R2_Out()
    {
        botones.R2_Down = false;
        anim.SetTrigger("R2_Out");
        anim.SetBool("R2_Down", false);
    }


    public virtual void Start()
    {
        if (actC != null) actC(cargaActual*100/3);
    }

    public virtual GameObject Crear(ref GameObject obj, Transform punto, bool AplicarAlChildren)
    {
        GameObject clone = Instantiate<GameObject>(obj, punto.transform.position, punto.transform.rotation);
        if (AplicarAlChildren)
        {
            clone.GetComponentInChildren<Proyectil>().creador = gameObject;
            clone.GetComponentInChildren<Proyectil>().tagDueño = gameObject.tag;
            clone.GetComponentInChildren<Proyectil>().Carga(cargaActual);
        }
        else
        {
            clone.GetComponent<Proyectil>().creador = gameObject;
            clone.GetComponent<Proyectil>().tagDueño = gameObject.tag;
            clone.GetComponent<Proyectil>().Carga(cargaActual);
        }
        CargandoUp();
        return clone;
    }

    public virtual void Spawn_Proyectil_Simple()
    {
        GameObject proyectil = Crear(ref proyectil_Simple, pDispDina, true);
        cargaActual = 0;
        Destroy(proyectil, 5f);
    }

    public virtual void CargandoDown()
    {
        if (actC != null) actC(cargaActual * 100 / 3);
        if (cargaActual >= cargaMax)
        {
            cargaActual = cargaMax;
        }
        else
        {
            cargaActual += cargaVel * Time.deltaTime;
        }
    }

    public virtual void CargandoUp()
    {
        cargaActual = 0;
    }

    public virtual void EnGrab(int x)
    {
        if (x == 1) anim.SetBool("EnGrab", true);
        else anim.SetBool("EnGrab", false);
    }

    public void SetDir(int x)
    {
        dirDesplazamiento = x;
    }
    public void SetDirY(int y)
    {
        dirDesplazamientoY = y;
    }


    public virtual void DespEspSalto(int id)
    {
        playerMov.DesplazamientoSimple(new Vector2 (DesplSalto[id].fuerza.x * dirDesplazamiento, DesplSalto[id].fuerza.y * dirDesplazamientoY), DesplSalto[id].tBloqueo);
        dirDesplazamiento = 1;
        dirDesplazamientoY = 1;
    }
    public virtual void DespEspDash(int id)
    {
        playerMov.DesplazamientoSimple(new Vector2(DesplDash[id].fuerza.x * dirDesplazamiento, DesplDash[id].fuerza.y * dirDesplazamientoY), DesplDash[id].tBloqueo);
        dirDesplazamientoY = 1;
        dirDesplazamiento = 1;
    }

    public virtual void GrabEnd()
    {
        gControl.DesemparentarEnemigo();  
    }

    public virtual void Grab(int x)
    {
        if(x==0) gControl.ActivarGrab(false);
        else gControl.ActivarGrab(true);

    }

    public virtual void MovReducidoLeve()
    {
        movReducido = true;
    }

    public virtual void MovReducidoTotal(int x)
    {
        if (x == 0)
        {
            movReducidoTotal = false;
        }
        else
        {
            movReducidoTotal = true;
        }
    }

    public virtual void MovBloqueadoT(float tiempo)
    {
        movBloqueado = true;
        contMovBloqueado = 0;
        tMovBloqueado = tiempo;
    }

    public virtual void DesbloquearCombo()
    {
        anim.SetBool("BloqueoDeCombo", false);
    }
}
