﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimpiarTriggersEnSalida : StateMachineBehaviour
{

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	//override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.ResetTrigger("Fire1_In");
        animator.ResetTrigger("Fire1_Out");
        animator.ResetTrigger("Fire2_In");
        animator.ResetTrigger("Fire2_Out");
        animator.ResetTrigger("Fire3_In");
        animator.ResetTrigger("Fire3_Out");
        animator.ResetTrigger("Jump_In");
        animator.ResetTrigger("Jump_Out");
        animator.ResetTrigger("KnockBack");
        animator.ResetTrigger("Interrumpido");
        animator.ResetTrigger("R1_In");
        animator.ResetTrigger("R1_Out");
        animator.ResetTrigger("L1_In");
        animator.ResetTrigger("L1_Out");
        animator.ResetTrigger("R2_In");
        animator.ResetTrigger("R2_Out");
        animator.ResetTrigger("L2_In");
        animator.ResetTrigger("L2_Out");

        // animator.SetBool("BloqueoDeCombo", false);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}
}
