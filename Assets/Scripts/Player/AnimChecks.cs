﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimChecks : MonoBehaviour {

    public Animator anim;
    public Controller2D controller;


    void LateUpdate()
    {
        anim.SetBool("EnPiso", controller.collisions.below);
        anim.SetBool("EnParedIzq", controller.collisions.left);
        anim.SetBool("EnParedDer", controller.collisions.right);
    }
}
