﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CtrlCapasBoneToChild : MonoBehaviour
{
    bool usarInversion = false;
    [SerializeField] bool usarEjeZ = false;
    public bool noAnimarCapa = false;

    bool mirandoDer = true;
    public MirarOponente mir;

    private int capaCentral = 7;
    public float capaLocal = 0;
    List<SpriteRenderer> cRenders;

    public int correcionEnRevez = 0;

    int dir = 1;
	void Start ()
    {        
        GetChildsRenders();
        if (usarEjeZ) UpdateChildsLayers();
        else UsarValorLocal();
    }
      
    
    void GetChildsRenders()
    {
        cRenders = new List<SpriteRenderer>();
        int cantChild = transform.childCount;

        for (int a = 0; a < cantChild; a++)
        {
            if(transform.GetChild(a).GetComponent<SpriteRenderer>() != null)
            {
                cRenders.Add(transform.GetChild(a).GetComponent<SpriteRenderer>());
            }
        }       
      
    }
	
    void UpdateChildsLayers()
    {
        if(cRenders.Count == 0)
        {
            this.enabled = false;
            return;
        }

        int dir = 1;
        if (!mirandoDer) dir = -1;

        for (int i = 0; i < cRenders.Count; i++)
        {            
            float z = ((transform.position.z * 10));
            z = capaCentral + z * dir;
            print(gameObject.name + ": " + z);
            cRenders[i].sortingOrder = (int)z;
        }
    }

    void UsarValorLocal()
    {        
        for (int i = 0; i < cRenders.Count; i++)
        {
            if (mirandoDer) cRenders[i].sortingOrder = capaCentral + (int)capaLocal * dir;
            else cRenders[i].sortingOrder = capaCentral + correcionEnRevez + (int)capaLocal * dir;
        }
    }


	void Update ()
    {
        if(noAnimarCapa)
        {
            return;
        }
        mirandoDer = mir.mirandoDer;

        if (usarInversion)
        {
            if (mirandoDer) dir = 1;
            else dir = -1;
        }

        if (usarEjeZ) UpdateChildsLayers();
        else UsarValorLocal();
    }

   void OnValidate()
    {
        Start();
        Update();
    }

}
