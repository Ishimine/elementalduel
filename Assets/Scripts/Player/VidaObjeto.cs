﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidaObjeto : Vida {

    Rigidbody2D rb;

    public bool canKnockBack = false;

    private void Awake()
    {
        vidaActual = vidaMax;        
        rb = GetComponent<Rigidbody2D>();
    }

    public override void KnockBack(Vector2 posImpacto, Vector2 fImpacto, float durBloqueo, int dis = 0)
    {
        if (canKnockBack)
        {
            float dir = transform.position.x - posImpacto.x;
            if (dir > 0) dir = 1;
            else dir = -1;
            rb.velocity = rb.velocity/2;
            rb.AddForce(fImpacto * dir, ForceMode2D.Impulse);
        }
    }

    public override void KnockBack(Vector2 fImpacto, float durBloqueo, int dis = 0)
    {
        if (canKnockBack)
        {
            rb.velocity = rb.velocity / 2;
            rb.AddForce(fImpacto, ForceMode2D.Impulse);
        }
    }
    public override void Muerto()
    {
        if (objMuerto != null) objMuerto();
        if (actVida != null) actVida(vidaActual);     
        //Animar Destruccion del objeto   
        Destroy(gameObject);
    }
}
