﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Collider2D))]
public class ChequeoDeTerreno : MonoBehaviour {


    Collider2D col;

    [Header("Chequeos de terreno")]
    public LayerMask collisionMask;
    private float verticalRaySpacing;
    private float horizontalRaySpacing;
    public int horizontalRayCount = 3;
    public int verticalRayCount = 3;
    public float bordeCor;
    public Vector2 offsetRayos = new Vector2(-0.5f,-0.5f);
    public float yRayLength = 0.15f;
    public float xRayLength = 0.15f;

    
    public bool enPiso;
    public bool enParedIzq;
    public bool enParedDer;

    private void Awake()
    {
        col = GetComponent<Collider2D>();
    }
    private bool EnParedIzq()
    {
        if (!enPiso)
        {
            float directionX = -1;                               //DireccionDeLosRayos -1 lado izquiero 1 lado derecho

            horizontalRaySpacing = (col.bounds.size.y / 2) / (horizontalRayCount - 1);                                                    //Calcular distancia entre rayos
            for (int i = 0; i < horizontalRayCount; i++)
            {
                Vector2 rayOrigin = new Vector2(col.bounds.min.x, col.bounds.min.y) - Vector2.right * offsetRayos.x;                                    //Ubicar primera punta
                rayOrigin += Vector2.up * (horizontalRaySpacing * i);                                                  //Sumar cantidades de distancia de acuerdo al numero del rayo
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, xRayLength, collisionMask);
                Debug.DrawRay(rayOrigin, Vector2.right * directionX * xRayLength, Color.red);
                if (hit)
                {
                    return hit;
                }
            }
            return false;
        }
        else
        {
            return false;
        }
    }
    private bool EnParedDer()
    {
        if (!enPiso)
        {
            float directionX = 1; //DireccionDeLosRayos -1 lado izquiero 1 lado derecho         

            horizontalRaySpacing = (col.bounds.size.y / 2) / (horizontalRayCount - 1);                                                //Calcular distancia entre rayos
            for (int i = 0; i < horizontalRayCount; i++)
            {
                Vector2 rayOrigin = new Vector2(col.bounds.max.x, col.bounds.min.y) + Vector2.right * offsetRayos.x;             //Ubicar primera punta
                rayOrigin += Vector2.up * (horizontalRaySpacing * i);                                                             //Sumar cantidades de distancia de acuerdo al numero del rayo
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, xRayLength, collisionMask);
                Debug.DrawRay(rayOrigin, Vector2.right * directionX * xRayLength, Color.red);
                if (hit)
                {
                    return hit;
                }
            }
        }
        {
            return false;
        }
    }
    private bool EnPiso()
    {
        verticalRaySpacing = col.bounds.size.x / (verticalRayCount - 1) - bordeCor;                                                  //Calcular distancia entre rayos

        for (int i = 0; i < verticalRayCount; i++)
        {
            Vector2 rayOrigin = new Vector2(col.bounds.min.x + bordeCor, col.bounds.min.y) + Vector2.up * offsetRayos.y;              //Ubicar primera punta
            rayOrigin += Vector2.right * (verticalRaySpacing * i);                                                  //Sumar cantidades de distancia de acuerdo al numero del rayo
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.down, yRayLength, collisionMask);
            Debug.DrawRay(rayOrigin, Vector2.down * yRayLength, Color.red);
            if (hit)
            {
                return hit;
            }
        }
        return false;
    }
    void Update()
    {
        enPiso = EnPiso();
        enParedIzq = EnParedIzq();
        enParedDer = EnParedDer();
    }
}
