﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabControl : MonoBehaviour {

    public Vector2 tamañoDelArea = new Vector2(1,1);

    public Animator anim;
    public Transform GrabedEnemy;
    public Transform enemyPos;
    string EnemyOrigName;
    string enemyChildName = "Enemy";
    GameObject enemy;

    Vector2 pA, pB;

    bool activo = false;
    bool aereo = false;

    public void FixedUpdate()
    {
        if(activo)
        {
            CheckGrabArea();
        }
    }

    public void Start()
    {
        CalcularArea();
    }

    bool EsValido(string tag)
    {
        if ((tag == "Player/1") || (tag == "Player/2") || (tag == "Player/3") || (tag == "Player/4"))
        {
            return true;
        }
        else
            return false;
    }

    public void ActivarGrab(bool x)
    {
        aereo = x;
        activo = true;
        StartCoroutine(GrabActivo());
    }

    void CalcularArea()
    {
        Vector3 mitad = tamañoDelArea /2;
        pA = transform.position + mitad;
        pB = transform.position - mitad;
    }

    void CheckGrabArea()
    {
        CalcularArea();
        Collider2D col = Physics2D.OverlapArea(pA, pB);
        if (col != null && EsValido(col.tag) && col.GetComponent<PlayerMovement>() != null)
        {
            EmparentarEnemigo(col.gameObject);
            activo = false;
        }
    }

    private void Update()
    {
        CalcularArea();
        Debug.DrawLine(pA, new Vector3(pB.x, pA.y));
        Debug.DrawLine(pA, new Vector3(pA.x, pB.y));
        Debug.DrawLine(pB, new Vector3(pA.x, pB.y));
        Debug.DrawLine(pB, new Vector3(pB.x, pA.y));
    }

    void EmparentarEnemigo(GameObject obj)
    {
        enemy = obj;
        if(aereo) enemy.GetComponent<PlayerMovement>().RecibirGrabAereo(enemyPos);
        else enemy.GetComponent<PlayerMovement>().RecibirGrab(enemyPos);
        anim.SetTrigger("Grab");        
    }

    IEnumerator GrabActivo()
    {        
        yield return new WaitForSeconds(.5f);
        activo = false;
    }

    public void DesemparentarEnemigo()
    {
        enemy.GetComponent<PlayerMovement>().TerminarGrab();
        enemy.GetComponent<Animator>().SetTrigger("KnockBackFuerte");
    }


}
