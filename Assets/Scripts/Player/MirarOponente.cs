﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirarOponente : MonoBehaviour {


    /// <summary>
    /// Si es verdadero, siempre se mirara al oponente
    /// </summary>
    bool apuntarSemiAuto = false;

    PlayerInput pInput;
    Animator anim;

    public GameObject cuerpo;
    public GameObject pivoteDin;
    public GameObject pivoteFijo;

    public bool mirandoDer = true;
    public Transform enemy;

    Vector2 axisIzq = new Vector2(0, 0);
    
    private void Start()
    {
        pInput = GetComponent<PlayerInput>();
        pInput.actStickIzq += Actualizaraxisizq;
        anim = GetComponent<Animator>();
    }

    public void ActivarMirSemiAuto(Transform enemy)
    {
        apuntarSemiAuto = true;
        this.enemy = enemy;
    }

    public void ActivarMirLibre()
    {
        apuntarSemiAuto = false;
    }

    public bool OponenteEnDer()
    {
        float dir = enemy.transform.position.x - transform.position.x;
        if (dir <= 0) return false;
        else   return true;
    }

    public void CambiarEnemy(Transform n)
    {
        enemy = n;
    }

    void Actualizaraxisizq(Vector2 axis)
    {
        axisIzq = axis;
    }

    void LateUpdate()
    {
        if (apuntarSemiAuto)
        {
            if(OponenteEnDer() && !mirandoDer && axisIzq.x > 0)
            {
                pivoteFijo.transform.rotation = new Quaternion(0, 0, 0, 0);
                cuerpo.transform.rotation = new Quaternion(0, 0, 0, 0);
                mirandoDer = true;
            }
            else if(!OponenteEnDer() && mirandoDer && axisIzq.x < 0)
            {               
                pivoteFijo.transform.rotation = new Quaternion(0, 180, 0, 0);
                cuerpo.transform.rotation = new Quaternion(0, 180, 0, 0);
                mirandoDer = false;
            }
            else if(mirandoDer)
            {
                pivoteFijo.transform.rotation = new Quaternion(0, 0, 0, 0);
                cuerpo.transform.rotation = new Quaternion(0, 0, 0, 0);
            }
            else
            {
                pivoteFijo.transform.rotation = new Quaternion(0, 180, 0, 0);
                cuerpo.transform.rotation = new Quaternion(0, 180, 0, 0);
            }

            if (mirandoDer && OponenteEnDer())
            {
                anim.SetFloat("MirandoOponenteFloat", 1);
            }
            else
            {
                anim.SetFloat("MirandoOponenteFloat", 0);
            }
        }
        else
        {
            if (axisIzq.x < 0) mirandoDer = false;
            else if (axisIzq.x > 0) mirandoDer = true;

            if (mirandoDer == false)
            {
                pivoteFijo.transform.rotation = new Quaternion(0, 180, 0, 0);
                cuerpo.transform.rotation = new Quaternion(0, 180, 0, 0);
            }
            else if(mirandoDer == true)
            {
                pivoteFijo.transform.rotation = new Quaternion(0, 0, 0, 0);
                cuerpo.transform.rotation = new Quaternion(0, 0, 0, 0);
            }          
        }




         
        if ((mirandoDer && axisIzq.x > 0) || (!mirandoDer && axisIzq.x < 0))
        {
            anim.SetFloat("Avanzando", 1);
        }
        else if ((mirandoDer && axisIzq.x < 0) || (!mirandoDer && axisIzq.x > 0))
        {
            anim.SetFloat("Avanzando", 0);
        }


        if (mirandoDer) anim.SetFloat("MirandoDer", 1);
        else anim.SetFloat("MirandoDer", 0);
    }

    public void Enfrentar()
    {
        if(OponenteEnDer())
        {
            pivoteFijo.transform.rotation = new Quaternion(0, 0, 0, 0);
            cuerpo.transform.rotation = new Quaternion(0, 0, 0, 0);
            mirandoDer = true;
        }
        else
        {
            pivoteFijo.transform.rotation = new Quaternion(0, 180, 0, 0);
            cuerpo.transform.rotation = new Quaternion(0, 180, 0, 0);
            mirandoDer = false;
        }
    }


    public bool getMirDer()
    {
        return mirandoDer;
    }
}
