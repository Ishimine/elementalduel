﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlDeCapas : MonoBehaviour
{
    bool usarInversion = false;
    public ImgCapa[] imagen;

    /// <summary>
    /// Capa alrededor de la cual todos los componentes van a ser ubicados. Un componente cuya capa es 1, se ubicara en capaCentral + 1
    /// </summary>
    public int capaCentral = 0;

    private void OnValidate()
    {
        ActualizarCapa();
    }

    void ActualizarCapa ()
    {
		for(int i = 0; i < imagen.Length; i++)
        {
            imagen[i].render.sortingLayerID = capaCentral + (int)imagen[i].capa;
        }
	}
	
	void Update ()
    {
        ActualizarCapa();		
	}
}
