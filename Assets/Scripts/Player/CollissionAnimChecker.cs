﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Controller2D))]
public class CollissionAnimChecker : MonoBehaviour {

    Controller2D ctrl2D;
    Animator anim;


	void Start ()
    {
        anim = GetComponent<Animator>();
        ctrl2D = GetComponent<Controller2D>();
    }
	
	void Update ()
    {
        anim.SetBool("EnPiso", ctrl2D.collisions.below);
        anim.SetBool("EnParedDer", ctrl2D.collisions.right);
        anim.SetBool("EnParedIzq", ctrl2D.collisions.left);
    }
}
