﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlDirPuntoDisparo : MonoBehaviour {

    public bool seguirEnemigo = false;
    public Transform tEnemigo;

    public PlayerInput pInput;
    Vector2 axis;
    Vector2 lastAxis;

    void Start()
    {
        if (!seguirEnemigo) pInput.actStickDer += ActualizarXY;
    }

    void ActualizarXY(Vector2 xy)
    {        
        lastAxis = axis;
        axis = xy;
    }

    void Update ()
    {
        if (seguirEnemigo)
        {
            if (tEnemigo == null)
            {
                Debug.LogWarning("No hay enemigo para aputanr ControlDirPuntoDist");
            }
            else
            {
                axis = tEnemigo.position - transform.position;
                axis.Normalize();
                float angle = Mathf.Atan2(axis.y, axis.x) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            }
        }
        else
        {
            if (lastAxis == axis)
            {
                return;
            }
            axis.Normalize();
            float angle = Mathf.Atan2(axis.y, axis.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
	}
}
