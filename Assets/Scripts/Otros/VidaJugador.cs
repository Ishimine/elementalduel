﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidaJugador : Vida {

    public float velMinimaImpacto = 20;

    SetDeMov setMov;
    PlayerMovement pMov;
    Controller2D ctrl2D;

    void Awake()
    {
        vidaActual = vidaMax;
        ctrl2D = GetComponent<Controller2D>();
        setMov = GetComponent<SetDeMov>();
        pMov = GetComponent<PlayerMovement>();
        ctrl2D.avisoGolpe += Impacto;
    }
    

    public override void KnockBack(Vector2 posImpacto, Vector2 fImpacto, float durBloqueo, int dir = 0)
    {
        setMov.MovBloqueadoT(durBloqueo);
        pMov.ActivarKnockBack(posImpacto, fImpacto, durBloqueo);
    }

    public override void KnockBack(Vector2 fImpacto, float durBloqueo, int dir = 0)
    {
        setMov.MovBloqueadoT(durBloqueo);
        pMov.ActivarKnockBack(fImpacto, durBloqueo);
    }

    public void Impacto(float x)
    {
        //print(x);
        if (Mathf.Abs(x) > velMinimaImpacto)
        {
            print("IMPACTO DE " + x);
        }
    }
        
    public override void Muerto()
    {
        Respawn();
        if (objMuerto != null) objMuerto();
        if (actVida != null) actVida(vidaActual);
    } 
    
    void Respawn()
    {
        gameObject.SetActive(false);
        gameObject.GetComponent<PlayerMovement>().resetVel();
        transform.position = new Vector3();
        vidaActual = vidaMax;
        gameObject.SetActive(true);
    }  
}
