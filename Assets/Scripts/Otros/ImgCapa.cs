﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ImgCapa : System.Object
{
    [SerializeField]
    public string nombrePieza;
    [SerializeField]
    public SpriteRenderer render;
    [SerializeField]
    public float capa;
}
