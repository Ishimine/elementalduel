﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proyectil_Tipo_B : Proyectil
{
    

    bool fImpactoDirFija = false;
  

    void OnCollisionEnter2D(Collision2D col)
    {
        if (EsImpactoValido(col.gameObject.tag))
        {
            //print(col.gameObject.name);
            var vida = col.gameObject.GetComponent<Vida>();
            if (vida != null)
            {
                int dir = -1;
                if (transform.parent.transform.rotation.y == 0) dir = 1;
                vida.RecibirDaño((int)daño, new Vector2(fImpacto.x*dir, fImpacto.y), durBloqueo, intensidad);
            }
            GetComponent<Collider2D>().isTrigger = true;
        }
    }
}
