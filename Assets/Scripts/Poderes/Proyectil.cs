﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proyectil : MonoBehaviour {

    public string tagDueño;
    public string[] tagsImpacto;

    [Range(1, 3)]
    public int intensidad = 1;



    public LayerMask capasImpacto;
    public LayerMask capasDueño;

    public GameObject creador;
    public float daño = 10;
    public float durBloqueo = 0.2f;

    public Vector2 fImpacto = new Vector2(10,5);


    public void Carga(float carga)
    {
        if (carga > 1)
        {
            daño *= carga;
            transform.localScale = transform.localScale + transform.localScale * carga / 10;
            fImpacto +=  fImpacto * carga / 1.5f;
            if(GetComponent<TrailRenderer>() != null)
            {
                GetComponent<TrailRenderer>().startWidth += GetComponent<TrailRenderer>().startWidth * carga / 10;
            }
        }
    }


    public bool EsImpactoValido(string tagX)
    {
        for(int i = 0; i < tagsImpacto.Length; i++)
        {
            if (tagX == tagsImpacto[i] && tagX != tagDueño) return true;
        }
        return false;
    }   

}
