﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailControl : MonoBehaviour
{
    public float tCorto = .1f;
    public float tLargo = .3f;

    public TrailRenderer _1_ManoDer;
    public TrailRenderer _2_ManoIzq;
    public TrailRenderer _3_PieIzq;
    public TrailRenderer _4_PieDer;


    void Awake ()
    {
        DesactivarTrail();
    }

    public void ActivarTrailLargo()
    {
        _1_ManoDer.time = tLargo;
        _2_ManoIzq.time = tLargo;
        _3_PieIzq.time = tLargo;
        _4_PieDer.time = tLargo;
    }    

    public void DesactivarTrail()
    {
        _1_ManoDer.time = 0;
        _2_ManoIzq.time = 0;
        _3_PieIzq.time = 0;
        _4_PieDer.time = 0;
    }    
}
