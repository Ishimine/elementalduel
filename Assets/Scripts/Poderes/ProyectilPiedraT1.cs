﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProyectilPiedraT1 : Proyectil
{
    public float vel = 30;
    Rigidbody2D rb;

    [SerializeField] bool isLetal = true;
    [SerializeField]
    Vector2 velAct;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.up * vel;
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (EsImpactoValido(col.gameObject.tag))
        {
            var hit = col.gameObject;
            var vida = hit.GetComponent<Vida>();
            if (vida != null)
            {
                vida.RecibirDaño((int)daño, col.contacts[0].point, fImpacto, durBloqueo,intensidad);
            }
            //Animacion Destruccion
            Destroy(gameObject);
        }
    }

    void FixedUpdate()
    {
        velAct = rb.velocity;
        CheckLetality();
    }


    void CheckLetality()
    {
        if(Mathf.Abs(rb.velocity.x) < 10 && Mathf.Abs(rb.velocity.y) < 10)
        {
            isLetal = false;
        }
        else
        {
            isLetal = true;
        }
    }
}