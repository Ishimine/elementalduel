﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColumnaDeAtaque_Tierra : Proyectil {
        

    
    public bool inAttack = true;
    

    public void InAttack(int x)
    {
        if (x == 0) inAttack = false;
        else inAttack = true;
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (inAttack && EsImpactoValido(col.gameObject.tag))
        {
            print(col.gameObject.name);
            var vida = col.gameObject.GetComponent<Vida>();
            if (vida != null)
            {
                vida.RecibirDaño((int)daño, creador.transform.position, fImpacto, durBloqueo,intensidad);
            }            
        }
    } 
}
