﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraDeBatalla : MonoBehaviour {

    public Transform objetivo1;
    public Transform objetivo2;

    private Vector3 rVel;
    public float smooth;
    public float maxSpeed = 10;
    public float distTamañoMin = 10;

    public Vector3 correccion = new Vector3(0, 2, -10);

    public Rect areaObj;

    private void Start()
    {
        if(objetivo2 == null)
        {
            Camera.main.orthographicSize = 20;
        }
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Alpha9))
        {
            ControlTiempo(2);
        }
        else if (Input.GetKeyUp(KeyCode.Alpha8))
        {
            ControlTiempo(0.5f);
        }
        if (objetivo1 != null && objetivo2 != null)
        {
            Vector3 posObj = objetivo1.transform.position + (objetivo2.position - objetivo1.position) / 2;


            transform.position = Vector3.SmoothDamp(transform.position, posObj + correccion, ref rVel, smooth, maxSpeed, Time.deltaTime);
            CalcularTamaño();
        }
        else if(objetivo1 != null)
        {
            transform.position = Vector3.SmoothDamp(transform.position, objetivo1.position + correccion, ref rVel, smooth, maxSpeed, Time.deltaTime);
        }
    }

    void CalcularTamaño()
    {
        float dist = Vector2.Distance(objetivo1.transform.position, objetivo2.transform.position) / 2f;
        if (dist > distTamañoMin)
        {
            GetComponent<Camera>().orthographicSize = Mathf.Lerp(GetComponent<Camera>().orthographicSize, dist, 0.3f);
        }
        else
        {
            GetComponent<Camera>().orthographicSize = Mathf.Lerp(GetComponent<Camera>().orthographicSize, distTamañoMin, 0.3f);
        }

    }

    void ControlTiempo(float x)
    {
        Time.timeScale = x * Time.timeScale;
    }
}
