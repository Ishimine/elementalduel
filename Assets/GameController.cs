﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	public enum TipoDeJuego {Solo, Duelo, TodosContraTodos};
    public TipoDeJuego Modo;

    PlayerConfig[] Jugadores;

    public LayerMask maskP_1;
    public LayerMask maskP_2;
    public LayerMask maskP_3;
    public LayerMask maskP_4;


    void Start()
    {
        if(Modo == TipoDeJuego.TodosContraTodos)
        {
            Debug.Log("Modo no habilitado, se cambio a modo Duelo");
            Modo = TipoDeJuego.Duelo;
        }

        buscarJugadores();

        switch (Modo)
        {
            case TipoDeJuego.Solo:
                break;
            case TipoDeJuego.Duelo:
                AplicarConfigDuelo();
                break;
            case TipoDeJuego.TodosContraTodos:
                break;
            default:
                break;
        }
    }


    void buscarJugadores()
    {
        Jugadores = FindObjectsOfType<PlayerConfig>();
    }

    void AplicarConfigSolo()
    {
        if (Jugadores.Length == 0)
        {
            Debug.LogError("No se encontraron jugadores. So boludo? pa que tocas play");
            Camera.main.GetComponent<CamaraDeBatalla>().enabled = false;
            return;
        }
        Camera.main.GetComponent<CamaraDeBatalla>().objetivo1 = Jugadores[0].transform;
        Jugadores[0].InicializarConfigJugador();

    }
    void AplicarConfigDuelo()
    {
        if(Jugadores.Length < 2)
        {
            Debug.LogError("No hay suficientes jugadores para la configuracion duelo, cambiando a SOLO");
            AplicarConfigSolo();
        }
        Jugadores[0].SetIdJugador(1);
        Jugadores[0].SetOponente(Jugadores[1].transform);
        Jugadores[0].SetMask(maskP_1);
        Jugadores[0].InicializarConfigJugador();
        Jugadores[0].transform.tag = "Player/1";
        Jugadores[0].gameObject.layer = 11;

        Jugadores[1].SetIdJugador(2);
        Jugadores[1].SetOponente(Jugadores[0].transform);
        Jugadores[1].SetMask(maskP_2);
        Jugadores[1].InicializarConfigJugador();
        Jugadores[1].transform.tag = "Player/2";
        Jugadores[1].gameObject.layer = 12;


        Camera.main.GetComponent<CamaraDeBatalla>().objetivo1 = Jugadores[0].transform;
        Camera.main.GetComponent<CamaraDeBatalla>().objetivo2 = Jugadores[1].transform;
    }
}
